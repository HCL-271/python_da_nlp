from peft import PeftModel
from transformers import LlamaTokenizer, LlamaForCausalLM, GenerationConfig
import textwrap
import torch
import gc
"""
YOU NEED TO INSTALL
НЕОБХОДИМО УСТАНОВИТЬ
!pip install -q datasets loralib sentencepiece
!pip -q install git+https://github.com/huggingface/peft.git
!pip -q install bitsandbytes
"""

class LlamaNN_Sum():
    
    def __init__(self, model_path, tokenizer_model, cuda=True, **kwargs):
        """
Инициализация модели с заданными весами и токенайзером
                Parameters
                ----------

                model_path: строка с путём к папке с файлами модели

                tokenizer_model: строка с путём к папке с файлами токенайзера
для модели,
                может совпадать с полем model

                Returns
                -------

                Ничего
        """
    
        self.tokenizer = LlamaTokenizer.from_pretrained(tokenizer_model)

        self.model = LlamaForCausalLM.from_pretrained(
            model_path,
            load_in_8bit=True,
            torch_dtype=torch.float16,
            device_map={'': 0}
        )

    def gen_ans(self, question, context) -> str :
        def alpaca_talk(text):
            inputs = self.tokenizer(
                text,
                return_tensors="pt",
            )

            input_ids = inputs["input_ids"].cuda()

            generation_config = GenerationConfig(
                temperature=0.3,
                top_p=0.95,
                repetition_penalty=1.2,
            )
            #print("Generating...")
            generation_output = self.model.generate(
                input_ids=input_ids,
                generation_config=generation_config,
                return_dict_in_generate=True,
                output_scores=True,
                max_new_tokens=256,
            )
            print("BRUHHHH")
            #print(generation_output.scores)
            decoded = ""
            for s in generation_output.sequences:
                decoded += self.tokenizer.decode(s)
            return decoded
        TF_ans = alpaca_talk(context[:2048])
        
        print("GENERATOR")
        print(TF_ans)
        print("GENERATRD")
        return TF_ans
    
    def combine_ans(self, question, context, str_len=1024, n=3, **kwargs) -> str :
        context_len = len(context)
        cross = 50
        if not question:
            question += " "
        arr_gen = []

        print("init_tf_combine")



        for i in range(int(context_len / (str_len * 2)) + 1):
            if i > n:
                break
            left_context = context[str_len * i : str_len * (i + 1)]
            textq = ""
            it1 ='''Внизу находится инструкция, описывающая вопрос. Напиши ответ на этот вопрос.

            ### Инструкция:
            ''' + left_context + textq+ "Напиши ответ на этот вопрос: "+question
            it2 = '''
            ### Ответ:
            '''
            left_context = it1 + it2


            
            if not left_context:
                left_context += " "
                
            #arr_context.append(left_context[:100])
            
            TF_ans = self.gen_ans(question, left_context)
            end_sentence = ''
            buff = TF_ans[:128].split('.')
        
            try:
                end_sentence = buff[0]
            except:
                end_sentence  = ''
            
            arr_gen.append(str(TF_ans)+str(end_sentence)+ '.')
            print(str_len)
            if (context_len <= str_len):
                break
            right_context = context[context_len-str_len*(i+1):context_len - str_len*i]
            if not right_context:
                right_context += " "
            
            #arr_context.append(right_context[:100])            
            TF_ans = self.gen_ans(question, right_context)
            end_sentence = ''
            buff = TF_ans[:128].split('.')
        
            try:
                end_sentence = buff[0]
            except:
                end_sentence  = ''
            arr_gen.append(str(TF_ans)+str(end_sentence)+ '.')
            #print(i)
            #print("comibined")

        #return arr_gen#, arr_context
        final_str= ""
        for i in range(len(arr_gen)):
            final_str +=str(arr_gen[i])+ "\n"
            


        return final_str

    def answer_tf(self, question, context, str_len=1024, n=3, **kwargs) -> str :

        """
Сгенерировать ответ моделью для заданных вопроса и контекста,
                с заданным токенайзером с генераторо TensorFlow
                Parameters
                ----------

                question: строка с вопросом для ответа

                context: строка с контекстом для ответа

                str_len: размер контекста для одной итерации

                n: число контекстов с краю

                Returns
                -------

                TF_ans : str
                строка - ответ от генератора от TensorFlow
        """

        docs = context.split("\n")
        #print(len(docs))
        modified_contx = [""]*(len(docs))
        #print(modified_contx)
        for i in range(len(docs)):

            if len(docs[i]) > str_len:
                modified_contx[i] = self.combine_ans(question, docs[i],str_len, n, **kwargs)
                #modified_contx[i] = docs[i]
            else:
                modified_contx[i] = docs[i]
            #print(docs[i][:100])
            #print("###########################################")
            #print(modified_contx[i][:100])
        arr_gen = []
        context_len = len(context)
        sum_len = 0
        for i in range(len(modified_contx)):
            if sum_len > 10000:
                break
            if (i > n):
                break
            left_context = modified_contx[i]
            textq = ""
            it1 ='''Внизу текст, содержащий ответ на вопрос. Напиши ответ на этот вопрос.

            ### Инструкция:
            ''' + left_context + textq+ "Напиши ответ на этот вопрос: "+question
            it2 = '''
            ### Ответ:
            '''
            left_context = it1 + it2
            s1 = self.gen_ans(question, left_context)
            #arr_gen.append(s1)

            res = s1.split("### Ответ:")
            #print(res)
            try:
                arr_gen.append(res[1])
                sum_len += len(res[1])
            except:
                arr_gen.append(res[0][len(left_context):])    
                sum_len += len(res[0][len(left_context):])
        print(arr_gen)

        final_str= ""
        final_save = ""
        print(arr_gen)
        if len(arr_gen) <= 3:
            for i in range(len(arr_gen)):
            
                final_str += "Ответ " + str(i) +  ":\n" + str(arr_gen[i])+ "\n"
                print("3-variant")
        else:
            for i in range(len(arr_gen)):
            
                final_str += "\n" + str(arr_gen[i])+ "\n"
            print(final_str)
            final_str =  "Ответ 3"+  ":\n" + self.gen_ans(question, final_str)
         
            final_save = "Ответ 1"+  ":\n" + str(arr_gen[0])+ "\n"+"Ответ 2"+  ":\n" + str(arr_gen[1])+ "\n"
            print(final_str)

        #del self.model_tf
        #del self.tokenizer
        #gc.collect()
        return final_save + final_str
    
    def answer(self, question, context, str_len=2048, n=5, **kwargs) -> str :
        """
Сгенерировать ответ моделью для заданных вопроса и контекста,
                с заданным токенайзером cо встроенным генератором
                Parameters
                ----------

                question: строка с вопросом для ответа

                context: строка с контекстом для ответа

                Returns
                -------

                basic_ans['answer'] : str
                строка - ответ от встроенного генератора

        """

        return None
