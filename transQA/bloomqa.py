from transformers import AutoModelForCausalLM, AutoTokenizer
from transformers import BloomForCausalLM
from transformers import BloomForTokenClassification
from transformers import BloomTokenizerFast, set_seed
import torch
import numpy as np
from transformers import AutoTokenizer, AutoModelForQuestionAnswering

class BloomNN():

    def __init__(self, model_path, tokenizer_model, cuda=True, **kwargs):
        """
Инициализация модели с заданными весами и токенайзером
                Parameters
                ----------

                model_path: строка с путём к папке с файлами модели

                tokenizer_model: строка с путём к папке с файлами токенайзера
для модели,
                может совпадать с полем model

                Returns
                -------

                Ничего
        """
        if cuda is True:
            #device = torch.device("cuda:0")
            self.model_lm = BloomForCausalLM.from_pretrained(model_path).cuda()
        
            self.model = AutoModelForCausalLM.from_pretrained(model_path).cuda()

        if cuda is False:
            self.model_lm = BloomForCausalLM.from_pretrained(model_path)
        
            self.model = AutoModelForCausalLM.from_pretrained(model_path)

        #self.tokenizer_bloom = BloomTokenizerFast.from_pretrained(tokenizer_model)
        self.tokenizer = AutoTokenizer.from_pretrained(tokenizer_model)

    
    def answer_tf(self, question, context, str_len=4000, n=10, **kwargs) -> str :

        """
Сгенерировать ответ моделью для заданных вопроса и контекста,
                с заданным токенайзером с генераторо TensorFlow
                Parameters
                ----------

                question: строка с вопросом для ответа

                context: строка с контекстом для ответа

                str_len: размер контекста для одной итерации

                n: число контекстов с краю

                Returns
                -------

                TF_ans : str
                строка - ответ от генератора от TensorFlow
        """
        #device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        device = torch.device("cuda:0")
        self.model = self.model.to(device)
        max_chunk_length = 200
        # Длина наложения
        overlapped_length = 30


        tokenized = self.tokenizer.encode_plus(
             question, context,
           add_special_tokens=False,
            
        )
        tokenized1 = self.tokenizer.encode_plus(
             question, 
           add_special_tokens=False
        )
        #print(tokenized)
        tokens = self.tokenizer.convert_ids_to_tokens(tokenized['input_ids'])
 
        #print(len(tokenized1["input_ids"]) )
        tok_quest = self.tokenizer.encode_plus(
             question, 
           add_special_tokens=False
        )
        #print(tok_quest)
        answer_tokens_length = tok_quest.input_ids.count(0)
        answer_input_ids = tokenized.input_ids[:answer_tokens_length]
        first_context_chunk_length = max_chunk_length - answer_tokens_length
        # Длина основного текста остальных блоков с наложением
        context_chunk_length = max_chunk_length - answer_tokens_length - overlapped_length
        # Токены основного текста
        context_input_ids = tokenized.input_ids[answer_tokens_length:]
        # Основной текст первого блока
        first = context_input_ids[:first_context_chunk_length]
        # Основной текст остальных блоков
        others = context_input_ids[first_context_chunk_length:]

        # Если есть блоки кроме первого
        # тогда обрабатываются все блоки
        if len(others) > 0:
          # Кол-во нулевых токенов, для выравнивания последнего блока по длине
          padding_length = context_chunk_length - (len(others) % context_chunk_length)
          others += [0] * padding_length

          # Кол-во блоков и их длина без добавления наложения
          new_size = (
              len(others) // context_chunk_length,
              context_chunk_length
          )

          # Упаковка блоков
          new_context_input_ids = np.reshape(others, new_size)

          # Вычисление наложения
          overlappeds = new_context_input_ids[:, -overlapped_length:]
          # Добавление в наложения частей из первого блока
          overlappeds = np.insert(overlappeds, 0, first[-overlapped_length:], axis=0)
          # Удаление наложение из последнего блока, так как оно не нужно
          overlappeds = overlappeds[:-1]

          # Добавление наложения
          new_context_input_ids = np.c_[overlappeds, new_context_input_ids]
          # Добавление первого блока
          new_context_input_ids = np.insert(new_context_input_ids, 0, first, axis=0)

          # Добавление вопроса в каждый блок
          new_input_ids = np.c_[
            [answer_input_ids] * new_context_input_ids.shape[0],
            new_context_input_ids
          ]
        # иначе обрабатывается только первый
        else:
          # Кол-во нулевых токенов, для выравнивания блока по длине
          padding_length = first_context_chunk_length - (len(first) % first_context_chunk_length)
          # Добавление нулевых токенов
          new_input_ids = np.array(
           [answer_input_ids + first + [0] * padding_length]
          )
        # Кол-во блоков
        count_chunks = new_input_ids.shape[0]

        # Маска, разделяющая вопрос и текст
        new_token_type_ids = [
          # вопрос блока
          [0] * answer_tokens_length
          # текст блока
          + [1] * (max_chunk_length - answer_tokens_length)
        ] * count_chunks
        #     Маска "внимания" модели на все токены, кроме нулевых в последнем блоке
        new_attention_mask = (
          # во всех блоках, кроме последнего, "внимание" на все слова
          [[1] * max_chunk_length] * (count_chunks - 1)
          # в последнем блоке "внимание" только на ненулевые токены
          + [([1] * (max_chunk_length - padding_length)) + ([0] * padding_length)]
        )
        # Токенизированный текст в виде блоков, упакованный в torch
        new_tokenized = {
         'input_ids': torch.tensor(new_input_ids).to(torch.int64).to(device),
         'attention_mask': torch.tensor(new_attention_mask).to(device)
        }

        outputs = self.model(**new_tokenized)
        print(outputs.hidden_states )
        # Позиции в 2D списке токенов начала и конца наиболее вероятного ответа
        # позиции одним числом
        #start_index = torch.argmax(outputs.start_logits)
        #end_index = torch.argmax(outputs.end_logits)

        start_index = torch.argmax(outputs.logits)
        end_index = torch.argmax(outputs.logits)
        # Пересчёт позиций начала и конца ответа для 1D списка токенов
        # = длина первого блока + (
        #   позиция - длина первого блока
        #   - длина ответов и отступов во всех блоках, кроме первого
        # )
        start_index = max_chunk_length + (
      start_index - max_chunk_length
      - (answer_tokens_length + overlapped_length)
      * (start_index // max_chunk_length)
        )
        end_index = 128 + max_chunk_length + (
      end_index - max_chunk_length
      - (answer_tokens_length + overlapped_length)
      * (end_index // max_chunk_length)
        )
        #print(tokens)
        # Составление ответа
        # если есть символ начала слова '▁', то он заменяется на пробел
        answer = ''.join(
          #[t.replace('▁', ' ') for t in tokens[start_index:end_index+1]] 
          [t.replace('▁', ' ') for t in tokens[0:100]]
        )

        print('Вопрос:', question)
        print('Ответ:', answer)
        return self.tokenizer.decode(answer)
        return None


    
    def answer(self, question, context, str_len=4000, n=10, **kwargs) -> str :
        """
Сгенерировать ответ моделью для заданных вопроса и контекста,
                с заданным токенайзером cо встроенным генератором
                Parameters
                ----------

                question: строка с вопросом для ответа

                context: строка с контекстом для ответа

                Returns
                -------

                basic_ans['answer'] : str
                строка - ответ от встроенного генератора

        """
        #context = context[:10000]
        arr_gen = []
        context_len = len(context)

        for i in range(int(context_len / (str_len * 2)) + 1):
            if (i > n):
                break
            left_context = context[str_len * i : str_len * (i + 1)]
            left_context = "Напиши ответ на вопрос: ###" +"\n" + question + "\n" +"### по тексту ###"+ "\n" +left_context + "\n" + "Напиши ответ на вопрос: (" + question+")"

            input_tokens = self.tokenizer(left_context, return_tensors="pt").to("cuda")
            result_sample = self.model.generate(**input_tokens, max_length=len(input_tokens['input_ids'][0]) + 200, **kwargs )
            res = self.tokenizer.decode(result_sample[0], truncate_before_pattern=[r"\n\n^#", "^'''", "\n\n\n"])
            #print(res)
            res = res.split("Напиши ответ на вопрос:")
            

            try:
                arr_gen.append(res[1])
            except:
                arr_gen.append(res[0][len(left_context):])

            if (context_len <= str_len):
                break

            right_context = context[context_len-str_len*(i+1):context_len - str_len*i]
            right_context = "Напиши ответ на вопрос: ###" +"\n" + question + "\n" +"### по тексту ###"+ "\n" +right_context + "\n" + "Напиши ответ на вопрос: (" + question+")"
            input_tokens = self.tokenizer(right_context , return_tensors="pt").to("cuda")
            result_sample = self.model.generate(**input_tokens, max_length=len(input_tokens['input_ids'][0]) + 200, **kwargs )
            res = self.tokenizer.decode(result_sample[0], truncate_before_pattern=[r"\n\n^#", "^'''", "\n\n\n"])
            
            res = res.split("Напиши ответ на вопрос:")
            
            try:
                arr_gen.append(res[1])
            except:
                arr_gen.append(res[0][len(right_context):])          
        final_str= ""
        for i in range(len(arr_gen)):
            final_str += "Ответ " + str(i) +  ":\n" + str(arr_gen[i])+ "\n"
            print(i)

        return final_str