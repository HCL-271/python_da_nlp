from transformers import pipeline
from transformers import AutoTokenizer
import tensorflow as tf
from transformers import TFAutoModelForQuestionAnswering
import gc
import time
class RuRoberta():

    def __init__(self, model_path, tokenizer_model, cuda=True, **kwargs):
        """
Инициализация модели с заданными весами и токенайзером
                Parameters
                ----------

                model_path: строка с путём к папке с файлами модели

                tokenizer_model: строка с путём к папке с файлами токенайзера
для модели,
                может совпадать с полем model

                Returns
                -------

                Ничего
        """
    
        self.tokenizer = AutoTokenizer.from_pretrained(tokenizer_model,
                                           use_fast=True, Truncate=True)
        self.model_tf = TFAutoModelForQuestionAnswering.from_pretrained(
                                                   model_path, from_pt=True)
        #self.question_answerer = pipeline("question-answering", str(model_path))

    def gen_ans(self, question, context) -> str :
        inputs = self.tokenizer(question, context, return_tensors="tf")
        outputs = self.model_tf(**inputs)
        asi = int(tf.math.argmax(outputs.start_logits, axis=-1)[0])
        aei = int(tf.math.argmax(outputs.end_logits, axis=-1)[0])
        
        predict_answer_tokens = inputs.input_ids[
                                              0, asi: aei + 128]
        
        TF_ans = self.tokenizer.decode(predict_answer_tokens)
        print("GENERATOR")
        print(TF_ans)
        print("GENERATRD")
        return TF_ans
    
    def combine_ans(self, question, context, str_len=4000, n=10, **kwargs) -> str :
        context_len = len(context)
        
        if not question:
            question += " "
        arr_gen = []

        print("init_tf_combine")
        for i in range(int(context_len / (str_len * 2)) + 1):
            #print(i*4096)
            if (i > n):
                break
            left_context = context[str_len * i : str_len * (i + 1)]
            
            if not left_context:
                left_context += " "
                
            #arr_context.append(left_context[:100])
            
            TF_ans = self.gen_ans(question, left_context)
            end_sentence = ''
            buff = TF_ans[:128].split('.')
        
            try:
                end_sentence = buff[0]
            except:
                end_sentence  = ''
            
            arr_gen.append(str(TF_ans)+str(end_sentence)+ '.')
            
            if (context_len <= str_len):
                break
            right_context = context[context_len-str_len*(i+1):context_len - str_len*i]
            if not right_context:
                right_context += " "
            
            #arr_context.append(right_context[:100])            
            TF_ans = self.gen_ans(question, right_context)
            end_sentence = ''
            buff = TF_ans[:128].split('.')
        
            try:
                end_sentence = buff[0]
            except:
                end_sentence  = ''
            arr_gen.append(str(TF_ans)+str(end_sentence)+ '.')
            print(i)
            print("comibined")

        #return arr_gen#, arr_context
        final_str= ""
        for i in range(len(arr_gen)):
            final_str +=str(arr_gen[i])+ "\n"
            


        return final_str



    def answer_tf(self, question, context, str_len=4000, n=10, **kwargs) -> str :

        """
Сгенерировать ответ моделью для заданных вопроса и контекста,
                с заданным токенайзером с генераторо TensorFlow
                Parameters
                ----------

                question: строка с вопросом для ответа

                context: строка с контекстом для ответа

                str_len: размер контекста для одной итерации

                n: число контекстов с краю

                Returns
                -------

                TF_ans : str
                строка - ответ от генератора от TensorFlow
        """
        docs = context.split("\n")
        print(len(docs))
        modified_contx = [""]*(len(docs))
        print(modified_contx)
        for i in range(len(docs)):
            
            if len(docs[i]) > str_len:
                modified_contx[i] = self.combine_ans(question, docs[i],str_len, n, **kwargs)
                
            else:
                modified_contx[i] = docs[i]
            print(docs[i][:100])
            print("###########################################")
            print(modified_contx[i][:100])
        context_len = len(context)
        for i in range(len(docs)):
            print(modified_contx[i][:100])
        print("Конец контекста")
        if not question:
            question += " "
        #print(context[:100])
        arr_gen = []
        #arr_context = []
        print("init_tf")
        for i in range(len(modified_contx)):
            #print(i*4096)
            #if (i > n):
            #    break
            left_context = modified_contx[i]
            
            if not left_context:
                left_context += " "
                
            #arr_context.append(left_context[:100])
            
            TF_ans = self.gen_ans(question, left_context)
            end_sentence = ''
            buff = TF_ans[:128].split('.')
        
            try:
                end_sentence = buff[0]
            except:
                end_sentence  = ''
            
            arr_gen.append(str(TF_ans)+str(end_sentence)+ '.')
            
            if (context_len <= str_len):
                break
            right_context = modified_contx[len(modified_contx) - i - 1]
            if not right_context:
                right_context += " "
            
            #arr_context.append(right_context[:100])            
            TF_ans = self.gen_ans(question, right_context)
            end_sentence = ''
            buff = TF_ans[:128].split('.')
        
            try:
                end_sentence = buff[0]
            except:
                end_sentence  = ''
            arr_gen.append(str(TF_ans)+str(end_sentence)+ '.')
            print(i)

        #return arr_gen#, arr_context
        final_str= ""
        final_save = ""
        print(arr_gen)
        if len(arr_gen) <= 3:
            for i in range(len(arr_gen)):
            
                final_str += "Ответ " + str(i) +  ":\n" + str(arr_gen[i])+ "\n"
                print("3-variant")
        else:
            for i in range(len(arr_gen)):
            
                final_str += "\n" + str(arr_gen[i])+ "\n"
            print(final_str)
            final_str =  "Ответ 3"+  ":\n" + self.gen_ans(question, final_str)
         
            final_save = "Ответ 1"+  ":\n" + str(arr_gen[0])+ "\n"+"Ответ 2"+  ":\n" + str(arr_gen[1])+ "\n"
            print(final_str)

        #del self.model_tf
        #del self.tokenizer
        #gc.collect()
        return final_save + final_str
    
    def answer(self, question, context, str_len=6000, n=10, **kwargs) -> str :
        """
Сгенерировать ответ моделью для заданных вопроса и контекста,
                с заданным токенайзером cо встроенным генератором
                Parameters
                ----------

                question: строка с вопросом для ответа

                context: строка с контекстом для ответа

                Returns
                -------

                basic_ans['answer'] : str
                строка - ответ от встроенного генератора

        """
        """
        arr_gen = []
        context_len = len(context)
        print("init")
        for i in range(int(context_len / (str_len * 2)) + 1):
            if (i > n):
                break
            left_context = context[str_len * i : str_len * (i + 1)]
            if not left_context:
                left_context += " "
            basic_ans = self.question_answerer(question=question, context=left_context)
            arr_gen.append(basic_ans['answer'])

            if (context_len <= str_len):
                break

            right_context = context[context_len-str_len*(i+1):context_len - str_len*i]
            
            if not right_context:
                right_context += " "
            basic_ans = self.question_answerer(question=question, context=left_context)
            arr_gen.append(basic_ans['answer'])
            print(i)
        print("gen")
        #return arr_gen
        final_str= ""
        for i in range(len(arr_gen)):
            final_str += "Ответ " + str(i) +  ":\n" + str(arr_gen[i])+ "\n"
            
        #del self.question_answerer 
        #del self.tokenizer
        #gc.collect()
        return final_str
        """
        return None