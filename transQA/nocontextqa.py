import torch
from transformers import T5ForConditionalGeneration as T5CG
from transformers import T5Tokenizer


class T5Gen():

    def __init__(self, model_path, tokenizer_model, cuda = True):
        self.tokenizer = T5Tokenizer.from_pretrained(tokenizer_model)

        if cuda is True:
            self.model = T5CG.from_pretrained(model_path).cuda()

        if cuda is False:
            self.model = T5CG.from_pretrained(model_path)

    
    def answer(self, question, context, str_len=16000, n=10, **kwargs) -> 'T5Gen':
        """
Cгенерировать ответ моделью для заданного вопроса
            Parameters
            ----------

            question: строка с вопросом для ответа

            model_path: строка с путём к папке с файлами модели

            tokenizer_model:  строка с путём к папке с файлами
токенайзера для модели, может совпадать с полем model

            cuda: булевский тип, вычислять ответ с использованием GPU
или без (False)

            Returns
            -------

            answer(question) : str
            строка - ответ от встроенного генератора
        """
        #context = context[:100000]
        def predict(x, **kwargs):
            inputs = self.tokenizer(x, return_tensors='pt').to(self.model.device)
            with torch.no_grad():
                hypotheses = self.model.generate(**inputs, **kwargs)
            return self.tokenizer.decode(hypotheses[0], skip_special_tokens=True)
        context_len = len(context)

        hint_str = "Тебе нужно ответить на ###ЗАПРОС### используя ###КОНТЕКСТ### "
        arr_gen = []
        #arr_context = []

        for i in range(int(context_len / (str_len * 2)) + 1):
            print(i*str_len)
            if (i > n):
                break
            left_context = context[str_len * i : str_len * (i + 1)]
            res = hint_str + " ###ЗАПРОС### " + question + " ###КОНТЕКСТ### " + left_context
            arr_gen.append(predict(res, **kwargs))
        #return arr_gen
        final_str= ""
        for i in range(len(arr_gen)):
            final_str += "Ответ " + str(i) +  ":\n" + str(arr_gen[i])+ "\n"
            print(i)
        #del self.model
        return final_str

    def answer_tf(self, question, context, str_len=16000, n=10, **kwargs) -> 'T5Gen':
        return None