from transformers import pipeline
from transformers import AutoTokenizer
import tensorflow as tf

import gc
import time
import torch
import numpy as np
from transformers import AutoTokenizer, AutoModelForQuestionAnswering
class DebertNN():

    def __init__(self, model_path, tokenizer_model, cuda=True, **kwargs):
        """
Инициализация модели с заданными весами и токенайзером
                Parameters
                ----------

                model_path: строка с путём к папке с файлами модели

                tokenizer_model: строка с путём к папке с файлами токенайзера
для модели,
                может совпадать с полем model

                Returns
                -------

                Ничего
        """
    
        self.tokenizer = AutoTokenizer.from_pretrained(tokenizer_model,
                                           use_fast=True, Truncate=True)
        #self.model_tf = TFAutoModelForQuestionAnswering.from_pretrained(
        #                                           model_path, from_pt=True)
        #self.model_pipeline = pipeline("question-answering", str(model_path))
        self.model = AutoModelForQuestionAnswering.from_pretrained(str(model_path))



    def answer_tf(self, question, context, str_len=8000, n=10, **kwargs) -> str :

        """
Сгенерировать ответ моделью для заданных вопроса и контекста,
                с заданным токенайзером с генераторо TensorFlow
                Parameters
                ----------

                question: строка с вопросом для ответа

                context: строка с контекстом для ответа

                str_len: размер контекста для одной итерации

                n: число контекстов с краю

                Returns
                -------

                TF_ans : str
                строка - ответ от генератора от TensorFlow
        """
        return None
    
    def answer(self, question, context, str_len=6000, n=10, **kwargs) -> str :
        """
Сгенерировать ответ моделью для заданных вопроса и контекста,
                с заданным токенайзером cо встроенным генератором
                Parameters
                ----------

                question: строка с вопросом для ответа

                context: строка с контекстом для ответа

                Returns
                -------

                basic_ans['answer'] : str
                строка - ответ от встроенного генератора

        """
        max_chunk_length = 512
        # Длина наложения
        overlapped_length = 30


        tokenized = self.tokenizer.encode_plus(
             question, context,
           add_special_tokens=False
        )
        tokenized1 = self.tokenizer.encode_plus(
             question, 
           add_special_tokens=False
        )
        #print(tokenized)
        tokens = self.tokenizer.convert_ids_to_tokens(tokenized['input_ids'])
 
        #print(len(tokenized1["input_ids"]) )
        answer_tokens_length = tokenized.token_type_ids.count(0)
        answer_input_ids = tokenized.input_ids[:answer_tokens_length]
        first_context_chunk_length = max_chunk_length - answer_tokens_length
        # Длина основного текста остальных блоков с наложением
        context_chunk_length = max_chunk_length - answer_tokens_length - overlapped_length
        # Токены основного текста
        context_input_ids = tokenized.input_ids[answer_tokens_length:]
        # Основной текст первого блока
        first = context_input_ids[:first_context_chunk_length]
        # Основной текст остальных блоков
        others = context_input_ids[first_context_chunk_length:]

        # Если есть блоки кроме первого
        # тогда обрабатываются все блоки
        if len(others) > 0:
          # Кол-во нулевых токенов, для выравнивания последнего блока по длине
          padding_length = context_chunk_length - (len(others) % context_chunk_length)
          others += [0] * padding_length

          # Кол-во блоков и их длина без добавления наложения
          new_size = (
              len(others) // context_chunk_length,
              context_chunk_length
          )

          # Упаковка блоков
          new_context_input_ids = np.reshape(others, new_size)

          # Вычисление наложения
          overlappeds = new_context_input_ids[:, -overlapped_length:]
          # Добавление в наложения частей из первого блока
          overlappeds = np.insert(overlappeds, 0, first[-overlapped_length:], axis=0)
          # Удаление наложение из последнего блока, так как оно не нужно
          overlappeds = overlappeds[:-1]

          # Добавление наложения
          new_context_input_ids = np.c_[overlappeds, new_context_input_ids]
          # Добавление первого блока
          new_context_input_ids = np.insert(new_context_input_ids, 0, first, axis=0)

          # Добавление вопроса в каждый блок
          new_input_ids = np.c_[
            [answer_input_ids] * new_context_input_ids.shape[0],
            new_context_input_ids
          ]
        # иначе обрабатывается только первый
        else:
          # Кол-во нулевых токенов, для выравнивания блока по длине
          padding_length = first_context_chunk_length - (len(first) % first_context_chunk_length)
          # Добавление нулевых токенов
          new_input_ids = np.array(
           [answer_input_ids + first + [0] * padding_length]
          )
        # Кол-во блоков
        count_chunks = new_input_ids.shape[0]

        # Маска, разделяющая вопрос и текст
        new_token_type_ids = [
          # вопрос блока
          [0] * answer_tokens_length
          # текст блока
          + [1] * (max_chunk_length - answer_tokens_length)
        ] * count_chunks

        #     Маска "внимания" модели на все токены, кроме нулевых в последнем блоке
        new_attention_mask = (
          # во всех блоках, кроме последнего, "внимание" на все слова
          [[1] * max_chunk_length] * (count_chunks - 1)
          # в последнем блоке "внимание" только на ненулевые токены
          + [([1] * (max_chunk_length - padding_length)) + ([0] * padding_length)]
        )
        # Токенизированный текст в виде блоков, упакованный в torch
        new_tokenized = {
         'input_ids': torch.tensor(new_input_ids),
         'token_type_ids': torch.tensor(new_token_type_ids),
         'attention_mask': torch.tensor(new_attention_mask)
        }

        outputs = self.model(**new_tokenized)
        #print(outputs.start_logits, torch.argmax(outputs.start_logits) )
        # Позиции в 2D списке токенов начала и конца наиболее вероятного ответа
        # позиции одним числом
        start_index = torch.argmax(outputs.start_logits)
        end_index = torch.argmax(outputs.end_logits)

        # Пересчёт позиций начала и конца ответа для 1D списка токенов
        # = длина первого блока + (
        #   позиция - длина первого блока
        #   - длина ответов и отступов во всех блоках, кроме первого
        # )
        start_index = max_chunk_length + (
      start_index - max_chunk_length
      - (answer_tokens_length + overlapped_length)
      * (start_index // max_chunk_length)
        )
        end_index = max_chunk_length + (
      end_index - max_chunk_length
      - (answer_tokens_length + overlapped_length)
      * (end_index // max_chunk_length)
        )

        # Составление ответа
        # если есть символ начала слова '▁', то он заменяется на пробел
        answer = ''.join(
          [t.replace('▁', ' ') for t in tokens[start_index:end_index+1]]
        )

        #print('Вопрос:', question)
        #print('Ответ:', answer)
        return answer