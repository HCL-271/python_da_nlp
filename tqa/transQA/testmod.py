
from transQA import T5Gen, RuRoberta, RuGPT3

if __name__=="__main__":
    model_checkpoint = "RoTrained/512"
    question  = "Когда надо приостанавливать с работником трудовой договор?"
    context = ""

    with open('answ1.txt', 'r',encoding = "utf-8" ) as f:
        for line in f:
            line = f.readline()
            context += line
	
    context += "Ответ:"
    a1 = RuRoberta(model_checkpoint,model_checkpoint)
    model_checkpoint = 'T5'
    a2 = T5Gen(model_checkpoint,model_checkpoint)
    model_checkpoint = "gptsmall"
    a3 = RuGPT3(model_checkpoint,model_checkpoint)

    
    print(a1.answer(question,context))
    print(a1.answer_tf(question,context))
    
    model_checkpoint = 'Kovr_/200T5' 
    print(a2.answer(question, ""))
    print(a2.answer_tf(question,""))
    model_checkpoint = "GptSmall"
    print(a3.answer(question, "",top_k = 0.7))
    print(a3.answer_tf(question, "",top_k = 0.7))
