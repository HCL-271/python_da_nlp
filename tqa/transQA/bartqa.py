import sentencepiece

from transformers import MBartTokenizer, MBartForConditionalGeneration

class BartNN():

    def __init__(self, model_path, tokenizer_model, cuda=True, **kwargs):
        """
Инициализация модели с заданными весами и токенайзером
                Parameters
                ----------

                model_path: строка с путём к папке с файлами модели

                tokenizer_model: строка с путём к папке с файлами токенайзера
для модели,
                может совпадать с полем model

                Returns
                -------

                Ничего
        """
        self.tokenizer = MBartTokenizer.from_pretrained(tokenizer_model)
        self.model = MBartForConditionalGeneration.from_pretrained(model_path)

    def gen_ans(self, question, context) -> str :
            input_ids = self.tokenizer(
    context,
    max_length=600,
    padding="max_length",
    truncation=True,
    return_tensors="pt",
            )["input_ids"]

            output_ids = self.model.generate(
    input_ids=input_ids,
    no_repeat_ngram_size=4
            )[0]


            
            res =  self.tokenizer.decode(output_ids, skip_special_tokens=True)
            return res
    
    def combine_ans(self, question, context, str_len=4000, n=2, **kwargs) -> str :
        context_len = len(context)
        
        if not question:
            question += " "
        arr_gen = []

        print("init_tf_combine")
        for i in range(int(context_len / (str_len * 2)) + 1):
            #print(i*4096)
            if (i > n):
                break
            left_context = context[str_len * i : str_len * (i + 1)]
            
            if not left_context:
                left_context += " "
                
            #arr_context.append(left_context[:100])
            
            TF_ans = self.gen_ans(question, left_context)
            end_sentence = ''
            buff = TF_ans[:128].split('.')
        
            try:
                end_sentence = buff[0]
            except:
                end_sentence  = ''
            
            arr_gen.append(str(TF_ans)+str(end_sentence)+ '.')
            
            if (context_len <= str_len):
                break
            right_context = context[context_len-str_len*(i+1):context_len - str_len*i]
            if not right_context:
                right_context += " "
            
            #arr_context.append(right_context[:100])            
            TF_ans = self.gen_ans(question, right_context)
            end_sentence = ''
            buff = TF_ans[:128].split('.')
        
            try:
                end_sentence = buff[0]
            except:
                end_sentence  = ''
            arr_gen.append(str(TF_ans)+str(end_sentence)+ '.')
            print(i)
            print("comibined")

        #return arr_gen#, arr_context
        final_str= ""
        for i in range(len(arr_gen)):
            final_str +=str(arr_gen[i])+ "\n"
            


        return final_str
    
    def answer_tf(self, question, context, str_len=8000, n=10, **kwargs) -> str :

        """
        Сгенерировать ответ моделью для заданных вопроса и контекста,
                с заданным токенайзером с генераторо TensorFlow
                Parameters
                ----------

                question: строка с вопросом для ответа

                context: строка с контекстом для ответа

                str_len: размер контекста для одной итерации

                n: число контекстов с краю

                Returns
                -------

                TF_ans : str
                строка - ответ от генератора от TensorFlow
        """
        docs = context.split("\n")
        print(len(docs))
        modified_contx = [""]*(len(docs))
        print(modified_contx)
        for i in range(len(docs)):
            
            if len(docs[i]) > str_len:
                modified_contx[i] = self.combine_ans(question, docs[i],str_len, n, **kwargs)
                
            else:
                modified_contx[i] = docs[i]
            print(docs[i][:100])
            print("###########################################")
            print(modified_contx[i][:100])
        arr_gen = []
        context_len = len(context)

        for i in range(len(modified_contx)):
            if (i > n):
                break
            left_context =  modified_contx[i]
            left_context = "Дан вопрос: ###" +"\n" + question + "\n" +"### по тексту ###"+ "\n" +left_context + "\n" + "Напиши по тексту ответ на вопрос: (" + question+")"
            res = self.gen_ans(question, left_context)
            
            arr_gen.append(res)


            if (context_len <= str_len):
                break

            right_context = modified_contx[len(modified_contx) - i - 1]
            right_context = "Дан вопрос: ###" +"\n" + question + "\n" +"### по тексту ###"+ "\n" +right_context + "\n" + "Напиши по тексту ответ на вопрос: (" + question+")"
            res = self.gen_ans(question, right_context)
            arr_gen.append(res)

        final_str= ""
        final_save = ""
        print(arr_gen)
        if len(arr_gen) <= 3:
            for i in range(len(arr_gen)):
            
                final_str += "Ответ " + str(i) +  ":\n" + str(arr_gen[i])+ "\n"
                print("3-variant")
        else:
            for i in range(len(arr_gen)):
            
                final_str += "\n" + str(arr_gen[i])+ "\n"
            print(final_str)
            final_str =  "Ответ 3"+  ":\n" + self.gen_ans(question, final_str)
         
            final_save = "Ответ 1"+  ":\n" + str(arr_gen[0])+ "\n"+"Ответ 2"+  ":\n" + str(arr_gen[1])+ "\n"
            print(final_str)

        #del self.model_tf
        #del self.tokenizer
        #gc.collect()
        return final_save + final_str

    
    def answer(self, question, context, str_len=3000, n=10, **kwargs) -> str :
        """
Сгенерировать ответ моделью для заданных вопроса и контекста,
                с заданным токенайзером cо встроенным генератором
                Parameters
                ----------

                question: строка с вопросом для ответа

                context: строка с контекстом для ответа

                Returns
                -------

                basic_ans['answer'] : str
                строка - ответ от встроенного генератора

        """
        docs = context.split("\n")
        print(len(docs))
        modified_contx = [""]*(len(docs))
        print(modified_contx)
        for i in range(len(docs)):
            
            if len(docs[i]) > str_len:
                modified_contx[i] = self.combine_ans(question, docs[i],str_len, n, **kwargs)
                
            else:
                modified_contx[i] = docs[i]
            print(docs[i][:100])
            print("###########################################")
            print(modified_contx[i][:100])
        #context = context[:10000]
        arr_gen = []
        context_len = len(context)

        for i in range(len(modified_contx)):
            if (i > n):
                break
            left_context =  modified_contx[i]
            #left_context = "Дан вопрос: ###" +"\n" + question + "\n" +"### по тексту ###"+ "\n" +left_context + "\n" + "Напиши по тексту ответ на вопрос: (" + question+")"
            res = self.gen_ans(question, left_context)
            
            arr_gen.append(res)


            if (context_len <= str_len):
                break

            right_context = modified_contx[len(modified_contx) - i - 1]
            #right_context = "Дан вопрос: ###" +"\n" + question + "\n" +"### по тексту ###"+ "\n" +right_context + "\n" + "Напиши по тексту ответ на вопрос: (" + question+")"
            res = self.gen_ans(question, right_context)
            
            arr_gen.append(res)

        final_str= ""
        final_save = ""
        print(arr_gen)
        if len(arr_gen) <= 3:
            for i in range(len(arr_gen)):
            
                final_str += "Ответ " + str(i) +  ":\n" + str(arr_gen[i])+ "\n"
                print("3-variant")
        else:
            for i in range(len(arr_gen)):
            
                final_str += "\n" + str(arr_gen[i])+ "\n"
            print(final_str)
            final_str =  "Ответ 3"+  ":\n" + self.gen_ans(question, final_str)
         
            final_save = "Ответ 1"+  ":\n" + str(arr_gen[0])+ "\n"+"Ответ 2"+  ":\n" + str(arr_gen[1])+ "\n"
            print(final_str)

        #del self.model_tf
        #del self.tokenizer
        #gc.collect()
        return final_save + final_str