from transformers import AutoModelForCausalLM
from transformers import pipeline
from transformers import AutoTokenizer

import torch
class RuGPT3():

    def __init__(self, model_path, tokenizer_model, cuda=True, **kwargs):
        """
Инициализация модели с заданными весами и токенайзером
                Parameters
                ----------

                model_path: строка с путём к папке с файлами модели

                tokenizer_model: строка с путём к папке с файлами токенайзера
для модели,
                может совпадать с полем model

                Returns
                -------

                Ничего
        """

        self.model = AutoModelForCausalLM.from_pretrained(model_path)
        self.tokenizer = AutoTokenizer.from_pretrained(
                                                 tokenizer_model,
                                                 use_fast=True, Truncate=True)
        self.question_answerer = pipeline("text-generation", model=str(model_path), **kwargs)

    
    def answer(self, question, context, str_len=2000, n=10, **kwargs) -> str:

        """
Cгенерировать продолжение моделью для заданного текста
        Parameters
        ----------

        prompt: строка с текстом для генерации

        Для подробного разъяснения следующих параметров смотри:
        https://huggingface.co/docs/transformers/main_classes/text_generation

        max_length: целое число - максимальная длина ответа

        max_new_tokens: целое число - максимальное число новых токенов в ответе

        do_sample: булевский тип - использование или нет сэмплинга

        top_k: целое число - наибольшая вероятность
для токенов из словаря пройти через top-k фильтрацию

        top_p: нецелое число от 0 до 1 - если < 1,
то только наименьший набор токенов, с вероятностью не меньшей top_p,
будет использован для генерации

        num_beams: целое число - число "лучей" для beam_search

        no_repeat_ngram_size: целое число - если больше 0,
то ngrams данного размера будут входить всего раз

        early_stopping: булевский тип - останавливает beam_search,
если число готовых предложений хотя бы num_beams

        skip_special_tokens: булевский тип - пропуск служебных токенов

        Returns
        -------

        s1 : str
        строка - ответ от встроенного генератора с дополнительными настройками:
max_new_tokens, do_sample, top_k, top_p

        """
        
        context_len = len(context)

        hint_str = "Тебе нужно ответить на ###ЗАПРОС### используя ###КОНТЕКСТ### "
        arr_gen = []
        #arr_context = []
        if (str_len == 0):
            str_len+=1
        for i in range(int(context_len / (str_len * 2)) + 1):
            print(i*str_len)
            if (i > n):
                break
            left_context = context[str_len * i : str_len * (i + 1)]
            print(left_context)
            res = hint_str + " ###ЗАПРОС### " + question + " ###КОНТЕКСТ### " + left_context
            s1 = self.question_answerer(res, **kwargs)

            arr_gen.append(s1[0]['generated_text'])
        #return arr_gen
        final_str= ""
        for i in range(len(arr_gen)):
            final_str += "Ответ " + str(i) +  ":\n" + str(arr_gen[i])+ "\n"
            print(i)
        #del self.model
        return final_str

    def answer_tf(self, question, context, **kwargs) -> str:

        return None
