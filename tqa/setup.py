#!/usr/bin/env python
from distutils.command.build_py import build_py as _build_py
from io import open
from setuptools import setup, find_packages
from os import getcwd, chdir, rename,listdir
from pathlib import Path


version = '0.0.19'

def RenameBitsandbytes():
    pth1 = getcwd()
    chdir("Lib")
    chdir("site-packages")
    pth2 = getcwd()
    path = Path(pth2)
    content = listdir(pth2)
    res = ""
    for way in content:
        if "_windows" in str(way):
            res = str(way)
            res = res.split("_windows")

    try:
        rename("bitsandbytes_windows" + res[1], "bitsandbytes"+res[1])
        print("RENAMING SUCCECFULL")
    except:
        print("RENAMING FAILED, PLEASE, INSTALL BITSANDBYTES-WINDOWS")
    chdir('../')
    chdir('../')


class BuildPyCmd(_build_py):

    def run(self):
        RenameBitsandbytes()
        _build_py.run(self)

with open('README.md', encoding='utf-8') as f:
    long_description = f.read()

with open("requirements.txt", encoding='utf-16') as f:
    reqs = f.readlines()

setup(
    name='transQA',
    version=version,

    author='KovrizhnykhDY',
    author_email='kovrizhnykhdyu@consultant.ru',

    description=(
        u'Lib for transformer NN question answering'

    ),
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=['transQA'],
    classifiers=[
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Programming Language :: Python :: Implementation :: CPython',
    ],
    install_requires=reqs,
    cmdclass={
            "build_py" : BuildPyCmd
        }
)
