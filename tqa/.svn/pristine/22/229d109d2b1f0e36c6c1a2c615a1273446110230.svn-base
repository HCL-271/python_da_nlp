from transformers import AutoModelForCausalLM
from transformers import pipeline
from transformers import AutoTokenizer

import torch
class RuGPT3():

    def __init__(self, model_path, tokenizer_model, cuda=True, **kwargs):
        """
Инициализация модели с заданными весами и токенайзером
                Parameters
                ----------

                model_path: строка с путём к папке с файлами модели

                tokenizer_model: строка с путём к папке с файлами токенайзера
для модели,
                может совпадать с полем model

                Returns
                -------

                Ничего
        """

        self.model = AutoModelForCausalLM.from_pretrained(model_path)
        self.tokenizer = AutoTokenizer.from_pretrained(
                                                 tokenizer_model,
                                                 use_fast=True, Truncate=True)
        self.question_answerer = pipeline("text-generation", model=str(model_path), **kwargs)

    
    def answer(self, question, context, **kwargs) -> str:

        """
Cгенерировать продолжение моделью для заданного текста
        Parameters
        ----------

        prompt: строка с текстом для генерации

        Для подробного разъяснения следующих параметров смотри:
        https://huggingface.co/docs/transformers/main_classes/text_generation

        max_length: целое число - максимальная длина ответа

        max_new_tokens: целое число - максимальное число новых токенов в ответе

        do_sample: булевский тип - использование или нет сэмплинга

        top_k: целое число - наибольшая вероятность
для токенов из словаря пройти через top-k фильтрацию

        top_p: нецелое число от 0 до 1 - если < 1,
то только наименьший набор токенов, с вероятностью не меньшей top_p,
будет использован для генерации

        num_beams: целое число - число "лучей" для beam_search

        no_repeat_ngram_size: целое число - если больше 0,
то ngrams данного размера будут входить всего раз

        early_stopping: булевский тип - останавливает beam_search,
если число готовых предложений хотя бы num_beams

        skip_special_tokens: булевский тип - пропуск служебных токенов

        Returns
        -------

        s1 : str
        строка - ответ от встроенного генератора с дополнительными настройками:
max_new_tokens, do_sample, top_k, top_p

        """

        s1 = self.question_answerer(question, **kwargs)

        return s1[0]['generated_text']

    def answer_tf(self, question, context, **kwargs) -> str:

        return None
