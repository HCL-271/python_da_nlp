import csv

from transQA import BartNN

from transformers import BartTokenizer, BartModel

tokenizer = BartTokenizer.from_pretrained('facebook/bart-base')
model = BartModel.from_pretrained('facebook/bart-base')


if __name__=="__main__":

    model_checkpoint = "facebook/bart-base"
    context = ""
    question = ""

    # open the file in the write mode
    cnt = open('cont_100.csv', 'r',encoding = "utf-8")
    cntr = csv.reader(cnt)

    s1=""

    #a1 = BartNN(model_checkpoint,model_checkpoint)
    i = 0
    with open('qe.txt', 'r',encoding = "utf-8" , newline = "\n") as f:
        for line in f:
            con = cnt.readline()
            i +=1
            #s1 = a1.answer(line ,con )
            inputs = tokenizer(line+" "+con, return_tensors="pt")
            outputs = model(**inputs)
            w = open('bart_contrast_promt_comp.csv', 'a',encoding="utf-8")
            writer = csv.writer(w)
            #res = str(s1).split("\n")

            #writer.writerow([''.join(res)])
            writer.writerow([''.join(outputs)])
            w.close()
            con = cnt.readline()


#a1 = RuRoberta(model_checkpoint,model_checkpoint)
#print(a1.answer_tf(question,context))
#print(a1.answer(question,context))

#a6 = BloomNN(model_checkpoint,model_checkpoint)
#print(a6.answer(question,context))
#print(a6.answer_tf(question,context))
